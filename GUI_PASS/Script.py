from tkinter import *
from flask import Flask
from flask_restful import Api, Resource
import sys
import glob
import serial
from Crypto import Random
from Crypto.Cipher import AES
import hashlib
import base64
import time

passwords = {}


class GetPass:
    @staticmethod
    def read_pass(slot, ser, decoder):
        ser.write([114, slot])  # trigger read command
        time.sleep(0.3)
        data = []
        while ser.in_waiting:
            data += [ser.read()]
        eeprom_str = ""
        for byte in data:
            eeprom_str += chr(int.from_bytes(byte, byteorder='little'))
        print(eeprom_str)
        passwords[slot] = decoder.decrypt(eeprom_str)

    def __init__(self, port, passwd):
        ser = serial.Serial(port, 9600, timeout=1)
        decoder = AESCipher(passwd)
        self.read_pass(1, ser, decoder)
        print(passwords[1])
        self.read_pass(3, ser, decoder)
        print(passwords[3])
        ser.close()


class AESCipher(object):

    def __init__(self, key):
        self.bs = 32
        self.key = hashlib.sha256(key.encode()).digest()

    def encrypt(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('UTF-8')

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]


def serial_ports():
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


class PassOne(Resource):
    def get(self):
        return {"pass": passwords[1]}  # get from serial connection here


class PassTwo(Resource):
    def get(self):
        return {"pass": passwords[3]}  # get from serial connection here

class StartDevice:
    def __init__(self, password, port):
        # do stuff
        self.password = password
        self.port = port
        self.app = Flask("WebServer")
        self.api = Api(self.app)
        self.api.add_resource(PassOne, '/pass1')
        self.api.add_resource(PassTwo, '/pass2')
        print("Running app")
        self.app.run(port='8080')


class PasswordPrompt:
    def __init__(self, master):
        self.options = serial_ports()
        self.master = master
        master.title("Unlock device")

        self.label = Label(master, text="Please enter password and select COM port")
        self.label.pack()

        self.var = StringVar()
        self.var.set("Select port...")
        self.port = OptionMenu(master, self.var, *self.options)
        self.port.pack()

        self.password = Entry(master, show="*", width=19)
        self.password.pack()

        self.enter_button = Button(master, text="Unlock device", command=self.unlock)
        self.enter_button.pack()

    def unlock(self):
        print("The entered password is:")
        print(self.password.get())
        print("The selected port is:")
        print(self.var.get())
        password = self.password.get()
        com_port = self.var.get()
        self.master.destroy()
        GetPass(com_port, password)
        StartDevice(password, com_port)


root = Tk()
my_gui = PasswordPrompt(root)
root.mainloop()
