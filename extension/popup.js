function fillUpPasword() {
	chrome.tabs.getSelected(null, function (tab) {
  	var url = new URL(tab.url)
  	var domain = url.hostname
  	var resp = ''
  	var xhr = new XMLHttpRequest()
  	var url_part = ""
  	if (url.hostname == "moodle.cs.utcluj.ro")
  		url_part = "pass1"
  	if (url.hostname == "twitter.com")
  		url_part = "pass2"
	xhr.open("GET", "http://127.0.0.1:8080/" + url_part, true);//change to dynamic URL
	xhr.onreadystatechange = () => {
		if (xhr.readyState == 4)
		{
	    	resp = xhr.responseText;//Got response from server
	    	//Set params for the executed script
	    	pass = JSON.parse(resp).pass
	    	chrome.tabs.executeScript(null,
	    		{code: "var currentInjectDomain = " + "'" + domain + "'"}, () => {
		  			chrome.tabs.executeScript(null, 
		  				{code: "var passwordRetrieved = " + "'" + pass + "'"}, () => {
		  					chrome.tabs.executeScript(null, {file: "inject.js"})
		  				})
		  			})
		}
	}
	xhr.send();
  })
}

function click(e) {
  	chrome.tabs.executeScript(null,
      {file: "jquery.js"}, () => {
      	fillUpPasword()
      })
  window.close();
}

document.addEventListener('DOMContentLoaded', function () {
  var divs = document.querySelectorAll('div');
  for (var i = 0; i < divs.length; i++) {
    divs[i].addEventListener('click', click);
  }
});
