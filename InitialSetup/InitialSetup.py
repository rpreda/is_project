from tkinter import *
from Crypto import Random
from Crypto.Cipher import AES
import sys
import glob
import serial
import hashlib
import base64
import time


class AESCipher(object):

    def __init__(self, key):
        self.bs = 32
        self.key = hashlib.sha256(key.encode()).digest()

    def encrypt(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]


def serial_ports():
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')
    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


class SetupPrompt:
    def __init__(self, master):
        self.options = serial_ports()

        self.var = StringVar()
        self.var.set("Select port...")
        self.port = OptionMenu(master, self.var, *self.options)
        self.port.pack()

        self.show = False
        self.master = master
        master.title("Device setup")

        self.label0 = Label(master, text="Please enter the master password")
        self.label0.pack()
        self.password_master = Entry(master, show="*", width=19)
        self.password_master.pack()

        self.label1 = Label(master, text="Please enter moodle password")
        self.label1.pack()
        self.password1 = Entry(master, show="*", width=19)
        self.password1.pack()

        self.label2 = Label(master, text="Please enter password #2")
        #self.label2.pack()
        self.password2 = Entry(master, show="*", width=19)
        #self.password2.pack()

        self.label3 = Label(master, text="Please twitter password")
        self.label3.pack()
        self.password3 = Entry(master, show="*", width=19)
        self.password3.pack()

        self.label4 = Label(master, text="Please enter password #4")
        #self.label4.pack()
        self.password4 = Entry(master, show="*", width=19)
        #self.password4.pack()

        self.enter_button = Button(master, text="Set device", command=self.submit)
        self.enter_button.pack()

    def submit(self):
        AESencrypter = AESCipher(self.password_master.get())
        pass1 = AESencrypter.encrypt(self.password1.get())
        pass2 = AESencrypter.encrypt(self.password2.get())
        pass3 = AESencrypter.encrypt(self.password3.get())
        pass4 = AESencrypter.encrypt(self.password4.get())
        ser = serial.Serial(self.var.get(), 9600, timeout=1)
        write_command = 119
        command_array = []
        command_array += [write_command] + [1] + [len(pass1)] + list(pass1)
        ser.write(command_array)

        command_array = []
        command_array += [write_command] + [3] + [len(pass3)] + list(pass3)
        time.sleep(0.1)
        ser.write(command_array)

        #command_array += [write_command] + [2] + [len(pass3)] + list(pass3)
        #command_array += [write_command] + [3] + [len(pass4)] + list(pass4)
        print(pass1)
        print(pass2)
        print(pass3)
        print(pass4)
        time.sleep(0.5)
        print(ser.read_all())
        ser.close()
        exit(0)


root = Tk()
gui = SetupPrompt(root)
root.mainloop()
