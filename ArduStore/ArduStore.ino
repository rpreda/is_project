#include <EEPROM.h>
#define IDTABLE_LEN 8
#define MAX_SLOT 4
/* Lookup table for the starts of memory regions in the eeprom (address starts for slots) */ 
unsigned char slot_starts[MAX_SLOT] = {200, 400, 600, 800};
/* Lookup table for lengths of the stored messages id_table[0] -> slot 0 and so on */
unsigned char id_table[IDTABLE_LEN];
/* General varaiblels */
unsigned char incomingByte;
/* Flags for controlling input */
unsigned char catch_slot;
unsigned char catch_len;
unsigned char catch_read_slot;
/* Global parameter values (command argument) */
unsigned char slot;
unsigned char m_len;//marks how many of the following bytes need to be written (length of the recieved message)
unsigned char index;
unsigned char read_slot;
/* Validate so slot isn't larger than MAX_SLOT */
unsigned char validateSlot(unsigned char slot)
{
  return slot;
}

void sendData(unsigned char slot)
{
  unsigned char len = id_table[slot];
  unsigned char buff;
  for (int i = 0; i < len; i++) 
  {
    EEPROM.get(slot_starts[slot] + i, buff);
    Serial.write(buff);
  }
}

void setup() {
  Serial.begin(9600);
  for (int i = 0; i < IDTABLE_LEN; i++)//Load data lookup table from eeprom
    EEPROM.get(i, id_table[i]);
  
}

void loop() {
  if (Serial.available() > 0)
  {
    incomingByte = Serial.read();
    if (!catch_read_slot && !catch_slot && !m_len && !catch_len)//command, doesn't have to catch the slot or catch the write data or catch the length
    {
      if (incomingByte == 119)//format: w<SLOT (0..4)><length of message, max 64> effect: writes the recieved message in eeprom at a certain slot
      {
        //write key to eeprom
        catch_slot = 1;
        index = 0;//reset index of that slot to 0
      }
      else if (incomingByte == 'r')//format: r<SLOT (0..4)> effect: sends via serial the data stored at slot number recieved
        catch_read_slot = 1;
      else 
        Serial.println("BAD COMMAND");
    }
    else if (catch_slot)//slot has to be 0, 1, 2, 3 I can send null :))
    {
     //after catching slot we need to catch len
      catch_slot = 0;
      slot = validateSlot(incomingByte);
      catch_len = 1;
    }
    else if (catch_len)
    {
      catch_len = 0;
      m_len = incomingByte;
      id_table[slot] = m_len;
      EEPROM.put(slot, m_len);//Write the lenght of the data in that slot
    }
    else if (m_len)
    {
      m_len--;
      EEPROM.put(slot_starts[slot] + index++, incomingByte);
    }
    else if (catch_read_slot)
    {
      catch_read_slot = 0;
      sendData(validateSlot(incomingByte));
    }
    else
      Serial.println("Error!");
  }
}
